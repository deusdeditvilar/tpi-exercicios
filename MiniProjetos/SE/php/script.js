$("#inputEmail2").on('change', function(){
    var email = $("#inputEmail").val();
    var confirma = $("#inputEmail2").val();
    
    if (email != confirma){
        $("#inputEmail2, #inputEmail").attr('class', 'form-control is-invalid');
    } else{
        $("#inputEmail2,#inputEmail").attr('class', 'form-control is-valid');
        $("#submit").removeAttr('disabled');
    }
})

function confirmacao() {
    // var nome = prompt("Qual o seu nome?");
    // confirm(`Boa noite, ${nome}!`);
    $("#textomodal").html(`Olá, nos informe o seu nome:  <input class="form-control" id="inputmodal">`)
    $("#exampleModal").modal('show');
    $("#inputmodal").on('change', function(){
        var input = $("#inputmodal").val();
        $("#enviar").removeAttr('disabled');
        $("#enviar").on('click',function(){
            // $("#exampleModal").modal('hide');
            $("#textomodal").html(`Boa noite, ${input}`)
            $("#enviar").attr('disabled', 'true');
            // $("#exampleModal").modal('show');
        })
    })
}



function alteraImagem() {
    var inicio = $("#inicio")
    if(inicio.css("background-image").match("i1.wp.com")){
        inicio.css("transition", "0.5s")
        inicio.css("background-image", `url("https://images2.alphacoders.com/970/thumb-1920-97040.jpg")`)
    }else{
        inicio.css("transition", "0.5s")
        inicio.css("background-image", `url("https://i1.wp.com/sorocabanices.com.br/wp-content/uploads/2019/08/Pizza-HD-Desktop-Wallpaper-15280_compress32.jpg?fit=3872%2C2592&ssl=1")`)
    }
}