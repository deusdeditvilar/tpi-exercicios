<!DOCTYPE html>
<html>
<?php include('conexao.php');
$dados = mysqli_query($conexao, "SELECT * FROM pessoas");
?>
<head>
    <title>Mini Projeto 3</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <link rel="stylesheet" href="bootstrap.min.css">
    <link rel="stylesheet" href="estilo.css">

</head>

<body onload="confirmacao()">
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="position: fixed;">
        <a class="navbar-brand" style="color: darkred;">Pizzaria de Deus</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Início</a>
            </div>
        </div>
    </nav>

    <div style="height: 100%;">
        <!-- <div class="bgimg-1" id="inicio" onclick="alteraImagem()">
            <h1 id="ini">Início</h1>
        </div> -->

        <div style="color: #777;background-color:white;text-align:center;padding:100px 80px;text-align: justify;">
            <h3 style="text-align:center;">Lista de Clientes</h3>
            <div style="margin-left:auto;margin-right:auto;" class="table-responsive col-sm-3">
            <table  class="table table-hover table-bordered table-condensed">
            <thead style="margin-left:auto;margin-right:auto;">
            <tr>
                <th class="col-sm-1">ID</th>
                <th class="col-sm-5">NOME</th>
                <th class="col-sm-5">EMAIL</th>
            </tr>
            </thead> 
            <?php 
                while($tabela=mysqli_fetch_array($dados)){
            ?>

            <tr>
                <td class="col-sm-1"><?php echo $tabela['id']; ?></td>
                <td class="col-sm-5" ><?php echo $tabela['nome']; ?></td>
                <td class="col-sm-5" ><?php echo $tabela['email']; ?></td>
            </tr>
            <?php
            }
            ?>
            </table>
            </div>
        </div>
    </div>





    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

</body>

</html>