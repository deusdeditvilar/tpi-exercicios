<!DOCTYPE html>
<html>

<head>
    <title>Mini Projeto 3</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <link rel="stylesheet" href="bootstrap.min.css">
    <link rel="stylesheet" href="estilo.css">

</head>

<body onload="confirmacao()">
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="position: fixed;">
        <a class="navbar-brand" style="color: darkred;">Pizzaria de Deus</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="#inicio">Início</a>
                <a class="nav-item nav-link" href="#sabores">Sabores</a>
                <a class="nav-item nav-link" href="#cadastro">Cadastre-se</a>
                <a class="nav-item nav-link" href="paineldecontrole.php">Painel de Controle</a>
            </div>
        </div>
    </nav>

    <div style="height: 100%;">
        <div class="bgimg-1" id="inicio" onclick="alteraImagem()">
            <h1 id="ini">Início</h1>
        </div>

        <div style="color: #777;background-color:white;text-align:center;padding:50px 80px;text-align: justify;">
            <h3 style="text-align:center;">Pizzaria de Deus</h3>
            <p>Quem não gosta de saborear uma deliciosa pizza? Seja ela com os amigos, família, ou até mesmo sozinho.
                Com a
                gente não é diferente.
                Gostamos de oferecer as melhores experiências para os nosso clientes que são amantes de pizza assim como
                nós.
            </p>
        </div>
    </div>


    <div class="bgimg-2" id="sabores">
        <h1 id="sabor">Sabores</h1>
    </div>

    <div style="color: #777;background-color:white;text-align:center;padding:50px 80px;text-align: justify;">
        <h3 style="text-align:center;">Sabores mais pedidos por aqui</h3>
        <ul class="nav" style="margin: 0rem 8.9rem 0rem 8.9rem;">
            <li class="nav-item">
                <a class="nav-link sab" target="_blank" title="Clique para visualizar"
                    href="https://cdn.awsli.com.br/600x450/824/824488/produto/33264733/cac776a726.jpg">Frango com
                    Catupiry</a>
            </li>
            <li class="nav-item">
                <a class="nav-link sab" target="_blank" title="Clique para visualizar"
                    href="https://pizzariadesucesso.com/wp-content/uploads/2018/05/pepperoni-pizza-1280x720.jpg" style="width: 100px;">Pepperoni</a>
            </li>
            <li class="nav-item">
                <a class="nav-link sab" target="_blank" title="Clique para visualizar"
                    href="https://media-cdn.tripadvisor.com/media/photo-s/0f/07/37/12/pizza-marguerita-mussarela.jpg">Marguerita</a>
            </li>
            <li class="nav-item">
                <a class="nav-link sab" target="_blank" title="Clique para visualizar"
                    href="https://3.bp.blogspot.com/-3Sp18F46Ltg/VyOruwQXTBI/AAAAAAAAAS8/iFvVup32Hew23yQj7vI9sBpo3uAfHFxqACKgB/s640/pizza-4queijos.jpg">Quatro
                    queijos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link sab" target="_blank" title="Clique para visualizar"
                    href="https://www.clonepizza.com.br/wp-content/uploads/PORTUGUESA-1.jpg">Portuguesa</a>
            </li>
            <li class="nav-item">
                <a class="nav-link sab" target="_blank" title="Clique para visualizar"
                    href="http://orgastronomico.whosthanny.com/wp-content/uploads/2016/11/Czar-Pizzas-Massas-Pizza-Doce-Dois-Amores.jpg">Dois
                    Amores</a>
            </li>
            <li class="nav-item">
                <a class="nav-link sab" target="_blank" title="Clique para visualizar"
                    href="https://media-cdn.tripadvisor.com/media/photo-s/10/a3/3d/d0/pizza-de-brigadeiro.jpg">Brigadeiro</a>
            </li>
        </ul>
    </div>

    <div class="bgimg-3" id="parte3">
        <h1 id="cadastro">Cadastre-se</h1>
    </div>


    <div style="color: #777;background-color:white;text-align:center;padding:50px 80px;text-align: justify;">
        <h3 style="text-align:center;">Cadastre-se para receber nossas notícias</h3>
        <form class="needs-validation" novalidate action="inserir.php" method="POST">
            <div class="form-row">
                <div class="col-md-4 mb-3 md-form">
                    <label for="validationCustom012">Email</label>
                    <input type="email" inputmode="email" name="email" class="form-control" id="inputEmail"
                        placeholder="Insira seu email" required>
                    <div class="invalid-feedback">
                        Emails diferem
                    </div>
                </div>
                <div class="col-md-4 mb-3 md-form">
                    <label for="validationCustom022">Confirma Email</label>
                    <input type="email" class="form-control" id="inputEmail2" placeholder="Confirme seu email" required>
                    <div class="invalid-feedback">
                        Emails diferem
                    </div>
                </div>
                <div class="col-md-4 mb-3 md-form">
                    <label for="validationCustomUsername2">Nome</label>
                    <input type="text" class="form-control" name="nome" id="validationCustomUsername2"
                        aria-describedby="inputGroupPrepend2" placeholder="Insira seu nome" required>
                </div>
            </div>
            <button class="btn btn-primary btn-sm btn-rounded" id="submit" type="submit" disabled="true">Enviar</button>
        </form>
    </div>

    <div class="bgimg-4" id="sobre"></div>


    <div style="color: #777;background-color:white;text-align:center;padding:50px 80px;text-align: center;">
        <h3>Feito e produzido por Deus<sup>TM</sup></h3>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Boa Noite!</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p id="textomodal"></p>
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              <button type="button" id="enviar" class="btn btn-primary" disabled="true">Enviar</button>
            </div>
          </div>
        </div>
      </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="script.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

</body>

</html>